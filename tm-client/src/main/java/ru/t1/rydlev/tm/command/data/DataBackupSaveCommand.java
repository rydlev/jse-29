package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpointClient().saveDataBackup(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save backup to file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
