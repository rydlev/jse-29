package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.TaskUpdateByIndexRequest;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

}