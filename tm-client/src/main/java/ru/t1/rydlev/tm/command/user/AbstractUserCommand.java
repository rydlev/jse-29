package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpointClient;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.exception.entity.UserNotFoundException;
import ru.t1.rydlev.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpointClient();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
