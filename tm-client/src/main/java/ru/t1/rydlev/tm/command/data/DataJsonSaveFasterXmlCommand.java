package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataJsonSaveFasterXmlRequest;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA SAVE JSON");
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest();
        getDomainEndpointClient().saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-json";
    }

}
