package ru.t1.rydlev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface ISystemEndpointClient extends ISystemEndpoint {

    void setSocket(@Nullable Socket socket);

}
