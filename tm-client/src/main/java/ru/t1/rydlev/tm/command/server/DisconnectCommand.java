package ru.t1.rydlev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server.";

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
        }
    }

}
