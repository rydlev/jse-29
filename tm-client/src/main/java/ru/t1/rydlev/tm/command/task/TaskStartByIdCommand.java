package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.TaskStartByIdRequest;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(id);
        getTaskEndpoint().startTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

}
