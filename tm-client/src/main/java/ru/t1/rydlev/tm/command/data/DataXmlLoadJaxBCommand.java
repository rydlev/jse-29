package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataXmlLoadJaxBRequest;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA LOAD XML");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest();
        getDomainEndpointClient().loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

}
