package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        getProjectEndpoint().clearProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

}
