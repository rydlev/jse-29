package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.TaskCreateRequest;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest();
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

}
