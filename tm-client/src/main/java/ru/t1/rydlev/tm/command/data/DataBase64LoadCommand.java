package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBase64LoadRequest;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA BASE64 LOAD");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        getDomainEndpointClient().loadDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from base64 file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
