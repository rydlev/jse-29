package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataYamlSaveFasterXmlRequest;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA SAVE YAML");
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest();
        getDomainEndpointClient().saveDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml";
    }

}
