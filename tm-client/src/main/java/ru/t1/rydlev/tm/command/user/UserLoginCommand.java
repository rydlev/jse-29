package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.UserLoginRequest;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        getAuthEndpoint().loginUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User login.";
    }

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
