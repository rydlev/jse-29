package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ProjectCompleteByIndexRequest;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().completeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

}
