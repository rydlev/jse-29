package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setId(id);
        getProjectEndpoint().startProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-id";
    }

}
