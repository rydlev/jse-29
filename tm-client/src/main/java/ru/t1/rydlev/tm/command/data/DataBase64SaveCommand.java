package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBase64SaveRequest;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA BASE64 SAVE");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpointClient().saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to base64 file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

}
