package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.UserViewProfileRequest;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @Nullable final User user = getAuthEndpoint().viewUserProfile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "view-user-profile";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
