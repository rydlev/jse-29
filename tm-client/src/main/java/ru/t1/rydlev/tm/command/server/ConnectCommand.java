package ru.t1.rydlev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.rydlev.tm.api.endpoint.IEndpointClient;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final IServiceLocator serviceLocator = getServiceLocator();
            @NotNull final IEndpointClient endpointClient = serviceLocator.getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();

            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
        }
    }

}