package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(id);
        getTaskEndpoint().completeTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

}
