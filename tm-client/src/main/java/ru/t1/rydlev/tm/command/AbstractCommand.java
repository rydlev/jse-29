package ru.t1.rydlev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.model.ICommand;
import ru.t1.rydlev.tm.api.service.IServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
