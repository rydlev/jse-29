package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint().showTaskByIndex(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

}
