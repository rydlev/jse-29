package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest();
        getDomainEndpointClient().loadDataBackup(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load backup from file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
