package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpointClient getTaskEndpoint() {
        return serviceLocator.getTaskEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final Task task : tasks) {
            @NotNull final String id = task.getId();
            @NotNull final String name = task.getName();
            @Nullable final String description = task.getDescription();
            @NotNull final String status = Status.toName(task.getStatus());
            @NotNull final String created = DateUtil.formatDate(task.getCreated());
            System.out.printf("%s. %s : %s : %s : %s; id: %s \n", index, name, status, created, description, id);
            index++;
        }
    }

}
