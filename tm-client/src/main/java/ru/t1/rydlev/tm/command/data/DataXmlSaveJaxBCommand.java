package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA SAVE XML");
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest();
        getDomainEndpointClient().saveDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

}
