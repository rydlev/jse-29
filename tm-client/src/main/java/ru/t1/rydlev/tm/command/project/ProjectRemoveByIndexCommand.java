package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().removeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

}
