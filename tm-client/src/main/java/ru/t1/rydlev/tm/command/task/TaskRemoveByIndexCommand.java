package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setIndex(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

}
