package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA SAVE BINARY");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        getDomainEndpointClient().saveDataBinary(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to binary file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

}
