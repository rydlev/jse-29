package ru.t1.rydlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.UserLoginRequest;
import ru.t1.rydlev.tm.dto.request.UserLogoutRequest;
import ru.t1.rydlev.tm.dto.request.UserViewProfileRequest;
import ru.t1.rydlev.tm.dto.response.UserLoginResponse;
import ru.t1.rydlev.tm.dto.response.UserLogoutResponse;
import ru.t1.rydlev.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse loginUser(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logoutUser(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse viewUserProfile(@NotNull UserViewProfileRequest request);

}
