package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
