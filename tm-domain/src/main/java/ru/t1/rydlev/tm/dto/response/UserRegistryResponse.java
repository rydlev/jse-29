package ru.t1.rydlev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
