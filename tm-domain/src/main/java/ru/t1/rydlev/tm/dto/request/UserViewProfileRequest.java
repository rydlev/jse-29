package ru.t1.rydlev.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {
}
