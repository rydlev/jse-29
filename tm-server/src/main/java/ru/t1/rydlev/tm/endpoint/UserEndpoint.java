package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpoint;
import ru.t1.rydlev.tm.api.service.IAuthService;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.api.service.IUserService;
import ru.t1.rydlev.tm.dto.response.*;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.model.User;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    private IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getNewPassword();
        getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final User user = getAuthService().registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse();
    }

}
