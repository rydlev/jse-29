package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rydlev.tm.api.service.IDomainService;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.dto.response.*;
import ru.t1.rydlev.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return this.getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
